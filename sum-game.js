/**
 * Created by CuongTV on 9/15/2016.
 * Usage example: node sum-game.js -i data/input6.txt -o output.txt
 * @params:
 *@-i : path of input file
 * @-o : path of output file
 */
var argv = require('minimist')(process.argv.slice(2));
if(!argv.i || !argv.o){
    return console.log('Plz provide input and output file!');
}
var fs = require('fs');
fs.readFile(argv.i, 'utf8', function (err,data) {
    if (err) {
        return console.log(err);
    }
    processContent(data);
});
function processContent(data){
    var inputNum = getInput(data);
    var results = [];
    if(!inputNum.testNumbers || inputNum.testNumbers.length == 0)
        return console.log("Can't get input");
    var partitionNumber = require('./partition-number.js');
    console.log(inputNum.testNumbers);
    var start = (new Date()).getTime();
    inputNum.testNumbers.forEach(function(val){
        results.push(partitionNumber(4, val));
    });
    console.log('Total spend: ' + ((new Date()).getTime() - start)/1000);
    console.log('Result: ' + results);
    writeToOutput(results);
}
function getInput(data){
    //get number of test case
    var numbers = data.split('\r\n');
    return {
        numOfTC: numbers[0],
        testNumbers: numbers.slice(1, numbers[0] + 1)
    };
}
function writeToOutput(dataArr){
    fs.writeFile(argv.o, dataArr.join('\r\n'), function (err) {
        if (err) return console.log("Can't writo to output file" + err);
        console.log('Everything done, plz check output at: ' + argv.o);
    });
}