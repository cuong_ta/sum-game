#Sum game
Count the number of ways to express n as sum of k positive numbers
###Install
npm install
###Usage Example
```
node sum-game.js -i data/input6.txt -o output.txt
```
where:
 ```data/input6.txt``` is input file, contains test case and list of number for calculation
 ```output.txt``` is output file, the result will be write to it.

