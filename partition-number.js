/**
 * Created by CuongTV on 9/15/2016.
 * Calculate the number of ways to express n as sum of k positive numbers
 * For ex: 5=1+1+1+2(a+b+c+d, a<=b<=b<=d)
 * @n is number need to calculate
 * @k is number of numbers need to calculate
 * @max is maximum number (d<=max) if max=null, there is no restrict.
 */
module.exports = function p(k, n, max){
    if(k > n) return 0;
    else if(k == n) return 1;
    //max first number ( number d in a+b+c+d) that we can start with
    var maxStart = n - (k - 1);//d reach max when: 1 + 1 + 1 + d for example
    var minStart = ~~(n/k)//d reach min when a=b=c=d for example, so this calculate floor of n/k
    var i = max > 0 && maxStart > max ? max : maxStart; //if maxStart > max, it should be max
    var total = 0;
    for (; i >= minStart; i--) {
        //calculate recursive
        /**p(k,n)   = {(n - 1) + number of way to write 1 with k-1 digits}
         *          + {(n - 2) + number of way to write 2 with k-1 digits}
         *          + {(n - 3) + number of way to write 3 with k-1 digits}
         *
         *          + {1 + number of way to write n - 1 with k-1 digits}
         */
        total += p(k - 1, n - i, i);
    }
    return total;
}
